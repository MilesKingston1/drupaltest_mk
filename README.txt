# The Kaplan Drupal test #


# Info files #
- Add qtip css/js files to themes .info file


# Modules #

Enable the following modules

- Administration menu
- Administration menu Toolbar style
- Custom message module
- Pathauto
- Token
- jQuery Update


# Content type #

Structure > Content Types > Add content type

Name > Kaplan

- Uncheck checkbox under Display settings > Display author and date information.
- Once created configure pathauto module, so that kaplan nodes just have "[node:title]" within their url.


# Create field Message #

Structure > Content Types > kaplan > Manage fields

- Add new field with the label message.
- Select field type text.


# Create kaplan template #

- Create a file in the templates directory of your theme called node--kaplan.tpl.php
- Find the variables for field each using print_r($node)
- add qTip functionality to body field